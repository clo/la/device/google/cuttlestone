# Building and running Cuttlestone

## Overview

The Cuttlestone target is derived from the Cuttlefish Android build
(cf_x86_phone). It is a virtual Android device, hosted in a VM which runs on the
[Google Compute Engine](https://console.cloud.google.com/compute). The normal
workflow for building and running Cuttlestone is:

1. (_Once per user_) Prepare your GCE environment
1. (_Once per organization_)  Build and publish an Android Virtual Device (AVD)
   GCE image
1. Build the cuttlestone_x86_phone Android target
1. (_Once per user_) Create an AVD GCE instance
1. Deploy the cuttlestone_x86_phone target to an AVD GCE instance
1. Connect to and debug the cuttlestone_x86_phone target

## Common variables

The sections below assume various common environment variables are available.
You can add these to your .bashrc, source a custom script, set them manually,
etc.

```shell
# GCE variables
export PROJECT=<your GCE project name, e.g. foobar>
export ZONE=<your GCE zone, e.g. us-central1-b>
export INSTANCE=<a unique name for your GCE AVD instance, e.g. foobar-cuttlestone-1>

# Android variables
# NOTE: Cuttlefish scripts (create_base_image.sh and upload_to_gce_and_run.py)
#   depend on this variable.
export ANDROID_BUILD_TOP=<absolute path to the root of the Android source, e.g. /path/to/source>
```

## Prepare your GCE environment

**NOTE: This step only needs to be performed once for each user**

In general, setting up your GCE environment is out-of-scope for this document.
Please see [this documentation](https://cloud.google.com/sdk/docs/initializing)
for more details. What follows are specific setup instructions for Cuttlestone:

* In the Google Cloud "APIs & Services" dashboard, make sure that the "Compute
  Engine" and "Storage" APIs are activated for your project
* Make sure your project has billing set up appropriately, as we will be
  creating VMs and using storage resources outside of the Free Tier
* Make sure to select a GCE zone which has the Intel Haswell (or newer) CPU
  architecture available. You can see the CPU architectures available in each
  zone [here](https://cloud.google.com/compute/docs/regions-zones/).
* The scripts in `device/google/cuttlefish_common/` assume that you have set the
  Google Cloud default project and zone:
  ```shell
  gcloud config set project ${PROJECT}
  gcloud config set compute/zone ${ZONE}
  ```
* To test your GCE setup, ensure that you can perform the following operations:
  * Create a GCE instance:
    ```shell
    gcloud compute instances create \
      --image-project=debian-cloud \
      --image-family=debian-9 \
      --machine-type=n1-standard-4 \
      --scopes storage-ro \
      --boot-disk-size=15GB \
      --min-cpu-platform="Intel Haswell" \
      --project=${PROJECT} \
      --zone=${ZONE} \
      test-instance
    ```
  * SSH to your GCE instance:
    ```shell
    gcloud compute ssh \
      --project=${PROJECT} \
      --zone=${ZONE} \
      test-instance
    ```
  * Delete your GCE instance:
    ```shell
    gcloud compute instances delete \
      --project=${PROJECT} \
      --zone=${ZONE} \
      test-instance
    ```

**NOTE**: The AVD GCE instances have two billing components: compute and
storage. You will be be billed for compute while your GCE instance is started.
While not in use, you can stop your GCE instances either via the Google Compute
dashboard, or using the `gcloud compute instances stop` command. You will be
billed for storage for as long as the disks for your GCE instance exist (in the
case of AVD GCE images, this is generally as long as the corresponding compute
instance exists). Please see the Google Cloud billing help for more details.

## Build and publish an Android Virtual Device (AVD) GCE image

**NOTE: This step can be performed by a single individual within your
organization and shared to all users**

Cuttlestone is hosted by a custom GCE image, based on Debian. The Keystone
source contains the necessary scripts to create this AVD GCE image.

1. Create a personal AVD GCE image `vsoc-host-scratch-${USER}` in your GCE
   project:
   ```shell
   cd ${ANDROID_BUILD_TOP}
   device/google/cuttlefish_common/tools/create_base_image.sh
   ```
   **NOTE**: This will take several minutes to run
2. (Optional) If you want to share this image across your whole organization,
   you should create a new image from this one, in a shared project:
   ```shell
   export SHARED_IMAGE_PROJECT=<your shared GCE project name, e.g. org-shared>
   export SHARED_IMAGE=<name of the image to create in the shared project, e.g. gce-avd-0-1>
   export SHARED_IMAGE_FAMILY=<family to which to add the image, e.g. gce-avd>
   gcloud compute images create \
     ${SHARED_IMAGE} \
     --project=${SHARED_IMAGE_PROJECT} \
     --family=${SHARED_IMAGE_FAMILY} \
     --source-image=vsoc-host-scratch-${USER} \
     --source-image-project=${PROJECT}
   ```
   Follow the directions
   [here](https://cloud.google.com/compute/docs/images/sharing-images-across-projects)
   to configure sharing of the new image.

## Build the cuttlestone_x86_phone Android target

Building the Android Cuttlestone target is much like building any other Keystone
target:
```shell
python development/keystone/container.py --android_target cuttlestone_x86_phone
source build/envsetup.sh
lunch cuttlestone_x86_phone-userdebug
make -j dist
exit
```

Key build artifacts:

* `out/dist/cuttlestone_x86_phone-img-eng.android-build.zip` - *.img files for
   the cuttlestone_x86_phone target (boot, cache, system, userdata, and vendor
   images)
* `out/dist/cvd-host_package.tar.gz` - Host binaries that run on the GCE
  instance

## Create an AVD GCE instance

**NOTE: This step only needs to be performed once per user (repeat whenever the
AVD GCE image is updated)**

Each user needs to create a GCE instance, running the AVD GCE image.

1. Create a new AVD GCE instance:
   ```shell
   export IMAGE_PROJECT=<the project containg the AVD GCE image created above>
   export IMAGE=<the image name of the AVD GCE image created above>
   gcloud compute instances create \
     --image-project=${IMAGE_PROJECT} \
     --image=${IMAGE} \
     --machine-type=n1-standard-4 \
     --scopes storage-ro \
     --boot-disk-size=15GB \
     --min-cpu-platform="Intel Haswell" \
     --project=${PROJECT} \
     --zone=${ZONE} \
     ${INSTANCE}
   ```
1. (_Temporary, 'acl' dependency is being added to AVD setup scripts_)
   ```shell
   gcloud compute ssh \
     --project=${PROJECT} \
     --zone=${ZONE} \
     vsoc-01@${INSTANCE} \
     -- \
     sudo apt install acl
   ```
1. If you aren't going to use the AVD GCE instance right now, you can shut it
   down:
   ```shell
   gcloud compute instances stop \
     --project=${PROJECT} \
     --zone=${ZONE} \
     ${INSTANCE}
   ```

## Deploy the cuttlestone_x86_phone target to an AVD GCE instance

1. Make sure that your AVD GCE instance is running:
   ```shell
   gcloud compute instances start \
     --project=${PROJECT} \
     --zone=${ZONE} \
     ${INSTANCE}
   ```
1. The Android source tree contains a script to deploy and launch a Cuttlestone
   build to an AVD GCE instance:
   ```shell
   cd ${ANDROID_BUILD_TOP}
   python device/google/cuttlefish_common/tools/upload_to_gce_and_run.py \
     -product cuttlestone_x86_phone \
     -instance ${INSTANCE}
   ```

You should see a stream of console messages. Once the device has booted
successfully, you should see a line like:
```
launch_cvd I 09-27 18:38:42  2757  2764 kernel_log_server.cpp:146] VIRTUAL_DEVICE_BOOT_COMPLETED
```
At this point, your Cuttlestone Android device is ready to use.

## Connect to and debug the cuttlestone_x86_phone target

### Interact with the Cuttlestone instance using VNC

The TightVNC Java viewer is recommended; it can be downloaded
[here](https://www.tightvnc.com/download.php).

To connect to a running Cuttlestone instance:

1. (_Once each time you start your AVD GCE instance_) Forward port 6444 (the AVD
   GCE instance VNC port):
   ```shell
   export LOCAL_VNC_PORT=6444
   gcloud compute ssh \
     --project=${PROJECT} \
     --zone=${ZONE} \
     vsoc-01@${INSTANCE} \
     -- \
     -L${LOCAL_VNC_PORT}:127.0.0.1:6444 -N -n -f
   ```
1. Connect to the forwarded VNC server:
   ```shell
   export TIGHTVNC_JVIEWER_DIR=<path to tightvnc jviewer installation>
   java -jar ${TIGHTVNC_JVIEWER_DIR}/tightvnc-jviewer.jar \
     -host=127.0.0.1 \
     -port=${LOCAL_VNC_PORT} \
     -showConnectionDialog=No \
     -ColorDepth=32
   ```

_TIP: If the Cuttlestone device display is larger than your physical display,
you can specify a scaling factor to TightVNC._

The keyboard shortcuts are:

* F5 = menu (unlock screen)
* F7 = power
* Ctrl+Left = landscape
* (Mac) Command+Left = landscape
* Ctrl+F11 = landscape
* Ctrl+Right = portrait
* (Mac) Command+Right = portrait
* Ctrl+F12 = portrait

### Interact with the Cuttlestone instance using ADB

#### Option 1: ADB from the AVD GCE instance

1. SSH into the AVD GCE instance:
   ```shell
   gcloud compute ssh --project=${PROJECT} --zone=${ZONE} vsoc-01@${INSTANCE}
   ```
1. Once connected, issue `adb` commands normally. For example,
   ```shell
   adb shell
   ```

#### Option 2: Forward the remote TCP/IP port to the local host

Forward port 6520 and issue an `adb connect` locally:
```shell
export LOCAL_ADB_PORT=6520
gcloud compute ssh \
  --project=${PROJECT} \
  --zone=${ZONE} \
  vsoc-01@${INSTANCE} \
  -- \
  -L${LOCAL_ADB_PORT}:127.0.0.1:6520 -N -n -f
adb connect 127.0.0.1:${LOCAL_PORT}
adb shell
```

### View Cuttlestone instance 'serial' logs

Cuttlestone devices have a virtual serial port, which can be viewed from the GCE
AVD instance:
```shell
gcloud compute ssh \
  --project=${PROJECT} \
  --zone=${ZONE} \
  vsoc-01@${INSTANCE} \
  -- \
  cat cuttlefish_runtime/kernel.log
```
